
import urllib, urllib2
import time

class Pushover:
	def __init__(self, APP_KEY, USER_KEY=None):
		self.API_URL = "https://api.pushover.net/1/messages.json"
		self.APP_KEY = APP_KEY
		self.user_key = USER_KEY
		self.msg_priority = 0
		self.msg_retry = 30
		self.msg_expire = 3600
		self.msg_sound = "siren"
		self.msg_title = None
		self.msg_body = None

	@property
	def priority(self):
		return self.msg_priority

	@priority.setter
	def priority(self, msg_priority):
		self.msg_priority = msg_priority

	@property
	def retry(self):
		return self.msg_retry

	@retry.setter
	def retry(self, msg_retry):
		self.msg_retry = msg_retry

	@property
	def expire(self):
		return self.msg_expire

	@expire.setter
	def expire(self, msg_expire):
		self.msg_expire = msg_expire

	@property
	def key(self):
		return self.user_key

	@key.setter
	def key(self, user_key):
		self.user_key = user_key

	@property
	def title(self):
		return self.msg_title

	@title.setter
	def title(self, msg_title):
		self.msg_title = msg_title

	@property
	def body(self):
		return self.msg_body

	@body.setter
	def body(self, msg_body):
		self.msg_body = msg_body

	@property
	def sound(self):
		return self.msg_sound

	@sound.setter
	def sound(self, msg_sound):
		self.msg_sound = msg_sound

	def debug(self):
		data = {
    		'token': self.APP_KEY,
    		'title': self.title,
    		'user': self.key,
    		'message': self.body,
    		'sound': self.sound,
    		'priority': self.priority,
    		'retry': self.retry,
    		'expire': self.expire,
    		'timestamp': int(time.time())
		}
		return data

	def send(self):

		data = urllib.urlencode({
    		'token': self.APP_KEY,
    		'title': self.title,
    		'user': self.key,
    		'message': self.body,
    		'sound': self.sound,
    		'priority': self.priority,
    		'retry': self.retry,
    		'expire': self.expire,
    		'timestamp': int(time.time())
		})

		req = urllib2.Request(self.API_URL)
		handle = urllib2.urlopen(req, data)
		handle.close()