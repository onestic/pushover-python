Pushover for Python
===================

A simplistic class in python allowing to send [pushover][1] messages from any other scripts
in an object oriented way.

Here is an example:

    :::python
       #!/usr/bin/env python
       # -*- coding: utf8 -*-

       from pushover import Pushover

       p = Pushover("YiE9pBLi3lTRZOsXYk0Uk89423kds")

       p.key = "X4Uk0pPKnw2oS7vsrMLHvt124lx"
       p.title = "Hello there!"
       p.body = "Just a test from a python script"

       # We use the Emergency Priority. Default: each 30 seconds for 1 hour
       p.priority = 2

       p.send()

Enjoy!

[1]:  https://pushover.net/
